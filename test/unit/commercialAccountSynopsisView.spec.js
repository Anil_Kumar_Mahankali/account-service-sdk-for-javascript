import CommercialAccountSynopsisView from '../../src/commercialAccountSynopsisView';
import dummy from '../dummy';

/*
test methods
 */
describe('CommercialAccountSynopsisView class', () => {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new CommercialAccountSynopsisView(null, dummy.address, dummy.id);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountSynopsisView(expectedName, dummy.address, dummy.id);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if address is null', () => {
            /*
             arrange
             */
            const constructor = () => new CommercialAccountSynopsisView(dummy.name, null, dummy.id);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'address required');
        });

        it('sets address', () => {
            /*
             arrange
             */
            const expectedAddress = dummy.address;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountSynopsisView(dummy.name, expectedAddress, dummy.id);

            /*
             assert
             */
            const actualAddress = objectUnderTest.address;
            expect(actualAddress).toEqual(expectedAddress);

        });
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor = () =>
                new CommercialAccountSynopsisView(dummy.name, dummy.address, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountSynopsisView(dummy.name, dummy.address, expectedId);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
    })
});
