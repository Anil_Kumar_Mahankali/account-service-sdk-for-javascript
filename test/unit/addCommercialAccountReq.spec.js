import AddCommercialAccountReq from '../../src/addCommercialAccountReq';
import dummy from '../dummy';

/*
tests
 */
describe('AddCommercialAccountReq class', () => {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        null,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.customerType,
                        dummy.customerSubType,
                        dummy.customerSubSubType,
                        dummy.customerBrand,
                        dummy.customerSubBrand
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');

        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    expectedName,
                    dummy.address,
                    dummy.phoneNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand
                );

            /*
             assert
             */
            const actualName =
                objectUnderTest.name;

            expect(actualName).toEqual(expectedName);

        });
        it('throws if address is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.name,
                        null,
                        dummy.phoneNumber,
                        dummy.customerType,
                        dummy.customerSubType,
                        dummy.customerSubSubType,
                        dummy.customerBrand,
                        dummy.customerSubBrand
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'address required');

        });
        it('sets address', () => {
            /*
             arrange
             */
            const expectedAddress = dummy.address;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.name,
                    expectedAddress,
                    dummy.phoneNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand
                );

            /*
             assert
             */
            const actualAddress =
                objectUnderTest.address;

            expect(actualAddress).toEqual(expectedAddress);

        });

        it('throws if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.name,
                        dummy.address,
                        null,
                        dummy.customerType,
                        dummy.customerSubType,
                        dummy.customerSubSubType,
                        dummy.customerBrand,
                        dummy.customerSubBrand
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'phoneNumber required');

        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.name,
                    dummy.address,
                    expectedPhoneNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand
                );

            /*
             assert
             */
            const actualPhoneNumber =
                objectUnderTest.phoneNumber;

            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);

        });

        /*it('throws if customerSegmentId & customerBrandId are both null', () => {
            /!*
             arrange
             *!/
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        null,
                        null
                    );

            /!*
             act/assert
             *!/
            expect(constructor).toThrowError(
                TypeError,
                'customerSegmentId or customerBrandId required'
            );

        });
        it('throws if customerSegmentId & customerBrandId both specified', () => {
            /!*
             arrange
             *!/
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        dummy.customerSegmentId,
                        dummy.customerBrandId
                    );

            /!*
             act/assert
             *!/
            expect(constructor).toThrowError(
                TypeError,
                'customerSegmentId and customerBrandId cannot both be specified'
            );

        });*/
        it('throws if customerType is null', () => {
            /*
             arrange
             */
            const constructor = () =>
                new AddCommercialAccountReq(
                    dummy.name,
                    dummy.address,
                    dummy.phoneNumber,
                    null,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'customerType required');
        });
        it('sets customerType',() => {
            /**
             * arrange
             */
            const expectedCustomerType = dummy.customerType;

            /**
             * act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.name,
                    dummy.address,
                    dummy.phoneNumber,
                    expectedCustomerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand
                );

            /**
             assert
             */
            const actualCustomerType
                = objectUnderTest.customerType;

            expect(actualCustomerType).toEqual(expectedCustomerType);
        });
        it('does not throw if customerBrand is null',() => {
            /**
             arrange
             */
            const constructor = () =>
              new AddCommercialAccountReq(
                  dummy.name,
                  dummy.address,
                  dummy.phoneNumber,
                  dummy.customerType,
                  dummy.customerSubType,
                  dummy.customerSubSubType,
                  null,
                  dummy.customerSubBrand
              );

            /**
             * act/assert
             */
            //expect(constructor).toThrowError(TypeError,'customerBrand required');
            expect(constructor).not.toThrow()
        });
        it('sets customerBrand',()=>{
            /**
             arrange
             */
            const expectedCustomerBrand = dummy.customerBrand;

            /**
             * act
             */
            const objectUnderTest
                = new AddCommercialAccountReq(
                dummy.name,
                dummy.address,
                dummy.phoneNumber,
                dummy.customerType,
                dummy.customerSubType,
                dummy.customerSubSubType,
                expectedCustomerBrand,
                dummy.customerSubBrand
                );

            /**
             * act
             */
            const actualCustomerBrand
                = objectUnderTest.customerBrand;

            expect(expectedCustomerBrand).toEqual(actualCustomerBrand);
        });

        /*it('sets customerBrandId', () => {
            /!*
             arrange
             *!/
            const expectedCustomerBrandId = dummy.customerBrandId;

            /!*
             act
             *!/
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    null,
                    expectedCustomerBrandId
                );

            /!*
             assert
             *!/
            const actualCustomerBrandId =
                objectUnderTest.customerBrandId;

            expect(actualCustomerBrandId).toEqual(expectedCustomerBrandId);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /!*
             arrange
             *!/
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    dummy.customerSegmentId
                );

            const expectedObject =
            {
                name: objectUnderTest.name,
                address: objectUnderTest.address.toJSON(),
                phoneNumber: objectUnderTest.phoneNumber,
                customerSegmentId: objectUnderTest.customerSegmentId,
                customerBrandId: null
            };

            /!*
             act
             *!/
            const actualObject =
                objectUnderTest.toJSON();

            /!*
             assert
             *!/
            expect(actualObject).toEqual(expectedObject);

        })*/
    });
});
