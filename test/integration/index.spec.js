import AccountServiceSdk,
{
    AddCommercialAccountReq,
    SearchCommercialAccountsAssociatedWithPartnerReq,
} from '../../src/index';
import {PostalAddress} from 'postal-object-model';
import CommercialAccountView from '../../src/commercialAccountView';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

/*
 test methods
 */
describe('Index module', () => {

    describe('default export', () => {
        it('should be AccountServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new AccountServiceSdk(config.accountServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(AccountServiceSdk));

        });
    });

    describe('instance of default export', () => {

        /*describe('addCommercialAccount method', () => {
            it('should return accountId', (done) => {

                 /!*
                 arrange
                 *!/
                const objectUnderTest
                    = new AccountServiceSdk(config.accountServiceSdkConfig);

                 /!*
                 act
                 *!/
                const accountIdPromise =
                    objectUnderTest.addCommercialAccount(
                        factory.constructValidAddCommercialAccountRequest(),
                        factory.constructValidAppAccessToken()
                    );

                /!**
                 * assert
                 *!/
                accountIdPromise
                    .then((accountId) => {
                        expect(accountId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));
            }, 20000);
        });*/

        /*describe('getCommercialAccountWithId method', () => {
            it('should return expected CommercialAccountView', (done) => {
                    /!*
                     arrange
                     *!/
                    let expectedCommercialAccountView;
                    let addCommercialAccountRequest =
                        factory.constructValidAddCommercialAccountRequest();

                    var requestObj =  new AddCommercialAccountReq(
                        dummy.name,
                        dummy.address.toJSON(),
                        dummy.phoneNumber,
                        dummy.customerType,
                        dummy.customerSubType,
                        null,
                        dummy.customerBrand,
                        null
                    );
                    const objectUnderTest =
                        new AccountServiceSdk(config.accountServiceSdkConfig);

                    // seed a new account so we can test the retrieval of it
                    const expectedAccountIdPromise =
                        objectUnderTest.addCommercialAccount(
                            requestObj,
                            factory.constructValidAppAccessToken()
                            )
                            .then(
                                (expectedAccountId) => {
                                    // construct expected account view
                                    expectedCommercialAccountView =
                                        new CommercialAccountView(
                                            config.existingPartnerAccountAssociation.partnerAccountId,
                                            dummy.name,
                                            dummy.address.toJSON(),
                                            null,
                                            dummy.customerType,
                                            dummy.customerSubType,
                                            null,
                                            dummy.customerBrand,
                                            null,
                                            dummy.phoneNumber
                                        );
                                    return expectedAccountId;
                                }
                            );

                    /!*
                     act
                     *!/
                    const actualCommercialAccountViewPromise =
                        expectedAccountIdPromise
                            .then((expectedAccountId)=> {
                                // get account
                                return objectUnderTest.getCommercialAccountWithId(
                                    expectedAccountId,
                                    factory.constructValidAppAccessToken()
                                )
                            });


                    /!*
                     assert
                     *!/
                    actualCommercialAccountViewPromise
                        .then((actualCommercialAccountView) => {
                            expect(actualCommercialAccountView).toBeTruthy();
                            //expect(actualCommercialAccountView).toEqual(expectedCommercialAccountView);
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));
                },
                20000);
        });*/

        /*describe('searchCommercialAccountsAssociatedWithPartner', () => {
            it('should return more than 0 results', (done) => {

                    /!*
                     arrange
                     *!/
                    const objectUnderTest =
                        new AccountServiceSdk(config.accountServiceSdkConfig);

                    const searchCommercialAccountsAssociatedWithPartnerReq =
                        new SearchCommercialAccountsAssociatedWithPartnerReq(
                            config.existingPartnerAccountAssociation.partialName,
                            config.existingPartnerAccountAssociation.partnerAccountId
                        );

                    /!*
                     act
                     *!/
                    const accountSynopsesPromise =
                        objectUnderTest.searchCommercialAccountsAssociatedWithPartner(
                            searchCommercialAccountsAssociatedWithPartnerReq,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                config.existingPartnerAccountAssociation.partnerAccountId
                            )
                        );

                    /!*
                     assert
                     *!/

                    accountSynopsesPromise
                        .then((accountSynopsis) => {
                            expect(accountSynopsis.length>=0).toBeTruthy();
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));
                },
                20000);
        });*/

        describe('addConsumerAccount method', () => {
            it('should return ConsumerAccountId', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest
                    = new AccountServiceSdk(config.accountServiceSdkConfig);

                /*
                 act
                 */
                const accountIdPromise =
                    objectUnderTest.addConsumerAccount(
                        factory.constructValidAddConsumerAccountRequest(),
                        factory.constructValidAppAccessToken()
                    );

                /**
                 * assert
                 */
                accountIdPromise
                    .then((ConsumerAccountId) => {
                        expect(ConsumerAccountId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));
            }, 20000);
        });

    });
});
