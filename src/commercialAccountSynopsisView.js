import {PostalAddress} from 'postal-object-model';

/**
 * The least detailed view of a account
 * @class CommercialAccountSynopsisView
 */
export default class CommercialAccountSynopsisView {

    _name:string;

    _address:PostalAddress;

    _id:string;

    constructor(name:string,
                address:PostalAddress,
                id:string) {

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

    }

    get name():string {
        return this._name;
    }

    get address():PostalAddress {
        return this._address;
    }

    get id():string {
        return this._id;
    }

    toJSON() {
        return {
            name: this._name,
            address: this._address.toJSON(),
            id: this._id
        }
    }

}
