import {inject} from 'aurelia-dependency-injection';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import CommercialAccountView from './commercialAccountView';
import CommercialAccountViewFactory from './commercialAccountViewFactory';

@inject(AccountServiceSdkConfig, HttpClient)
class GetCommercialAccountWithIdFeature {

    _config:AccountServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets the account with the provided id
     * @param {string} id
     * @param {string} accessToken
     * @returns {Promise.<CommercialAccountView>}
     */
    execute(id:string,
            accessToken:string):Promise<CommercialAccountView> {

        return this._httpClient
            .createRequest(`commercial-accounts/${id}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then((response) => CommercialAccountViewFactory.construct(response.content));
    }
}

export default GetCommercialAccountWithIdFeature;