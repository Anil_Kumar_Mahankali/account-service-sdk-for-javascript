import {inject} from 'aurelia-dependency-injection';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AddConsumerAccountReq from './addConsumerAccountReq';
import ConsumerAccountId from './consumerAccountId';

@inject(AccountServiceSdkConfig, HttpClient)
class AddConsumerAccountFeature {

    _config:AccountServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Adds a account
     * @param {AddConsumerAccountReq} request
     * @param {string} accessToken
     * @returns {Promise.<ConsumerAccountId>} id
     */
    execute(request:AddConsumerAccountReq,
            accessToken:string):Promise<ConsumerAccountId> {

        return this._httpClient
            .createRequest('consumer-accounts')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then((response) => response.content)
    }
}

export default AddConsumerAccountFeature;