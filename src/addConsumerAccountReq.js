import {PostalAddress} from 'postal-object-model';

/**
 * @class {AddCommercialAccountReq}
 */
export default class AddConsumerAccountReq {

    _firstName:string;

    _lastName:string;

    _address:PostalAddress;

    _phoneNumber:string;

    _personEmail:string;

    constructor(firstName:string,
                lastName:string,
                address:PostalAddress,
                phoneNumber:string,
                personEmail:string){

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        this._phoneNumber = phoneNumber;

        this._personEmail = personEmail;

    }


    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {PostalAddress}
     */
    get address():PostalAddress {
        return this._address;
    }

    /**
     * @returns {string}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {string}
     */
    get personEmail(): string{
        return this._personEmail;
    }

    toJSON() {
        return {
            firstName: this._firstName,
            lastName: this._lastName,
            address: this._address,
            phoneNumber: this._phoneNumber,
            personEmail: this._personEmail
        };
    }

}
