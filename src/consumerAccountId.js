
/**
 * @class {ConsumerAccountId}
 */
export default class ConsumerAccountId {

    _id:string;

    constructor(
        id:string
    ){

        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

    }

    /**
     * getter methods
     */
    get id():string{
        return this._id;
    }

    toJSON() {
        return {
            id: this._id
        }
    }

}


