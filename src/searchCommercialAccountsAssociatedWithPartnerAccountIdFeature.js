import {inject} from 'aurelia-dependency-injection';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import CommercialAccountSynopsisView from './commercialAccountSynopsisView';
import CommercialAccountSynopsisViewFactory from './commercialAccountSynopsisViewFactory';

@inject(AccountServiceSdkConfig, HttpClient)
class SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature {

    _config:AccountServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Searches the accounts associated with the provided partner SAP account number
     * by partial account name match
     * @param partnerAccountId
     * @param accessToken
     * @returns {Promise.<CommercialAccountSynopsisView[]>}
     */
    execute(partnerAccountId:string,
            accessToken:string):Promise<Array> {

        return this._httpClient
            .createRequest(`/commercial-accounts/partner-account-id/${partnerAccountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        CommercialAccountSynopsisViewFactory.construct(contentItem)
                )
            );
    }
}

export default SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature
