import CommercialAccountSynopsisView from './commercialAccountSynopsisView';
import {PostalAddressFactory} from 'postal-object-model';

export default class CommercialAccountSynopsisViewFactory {

    static construct(data):CommercialAccountSynopsisView {

        const name = data.name;

        const address = PostalAddressFactory.construct(data.address);

        const id = data.id;

        return new CommercialAccountSynopsisView(
            name,
            address,
            id
        );

    }

}
